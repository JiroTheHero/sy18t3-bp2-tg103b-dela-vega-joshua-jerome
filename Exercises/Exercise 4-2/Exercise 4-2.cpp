#include "pch.h"
#include "iostream"
#include "conio.h"
#include "time.h"
#include "string"

using namespace std;

class Spell
{
public:
	string name;
	int manaCost;
	int damage;
	int minDmg, maxDmg;

	void getDamage(Spell* spell)
	{
		this->damage = this->minDmg + (rand() % (this->maxDmg - this->minDmg) + 1);
	}
};

class Wizard
{
public:
	string name;
	int hp;
	int mana;

	void castSpell(Wizard* target, Spell* spell)
	{
		cout << this->name << " casts " << spell->name << " to attack " << target->name << endl;
		cout << this->name << " deals " << spell->damage << "!" << endl;
		this->mana -= spell->manaCost;
		target->hp -= spell->damage;
		_getch();
	}
};

int main()
{
	srand(time(NULL));

	Wizard *wiz1 = new Wizard;
	wiz1->hp = 150;
	wiz1->mana = 200;

	Wizard *wiz2 = new Wizard;
	wiz2->hp = 150;
	wiz2->mana = 200;

	Spell *fireball = new Spell;
	fireball->name = "Fireball";
	fireball->manaCost = 10;
	fireball->minDmg = 20;
	fireball->maxDmg = 40;

	cout << "Enter the name of the first wizard: ";
	cin >> wiz1->name;
	cout << "Enter the name of the second wizard: ";
	cin >> wiz2->name;

	int firstAtk = rand() % 2 + 1;

	if (firstAtk == 1)
	{
		cout << wiz1->name << " attacks first!\n";
	}
	else
	{
		cout << wiz2->name << " attacks first!\n";
	}
	cout << endl;
	_getch();

	while ((wiz1->hp > 0 && wiz2->hp > 0) && (wiz1->mana > 0 && wiz2->mana > 0))
	{
		cout << wiz1->name << endl;
		cout << "HP: " << wiz1->hp << endl;
		cout << "Mana: " << wiz1->mana << endl;
		cout << wiz2->name << endl;
		cout << "HP: " << wiz2->hp << endl;
		cout << "Mana: " << wiz2->mana << endl;

		cout << endl;
		_getch();

		if (firstAtk == 1)
		{
			fireball->getDamage(fireball);
			wiz1->castSpell(wiz2, fireball);

			firstAtk++;
		}
		else
		{
			fireball->getDamage(fireball);
			wiz2->castSpell(wiz1, fireball);

			firstAtk--;
		}

		cout << endl;
		system("cls");
	}

	if (wiz1->hp > 0 && wiz2->hp <= 0)
	{
		cout << wiz2->name << " has been defeated!\n";
		cout << wiz1->name << " wins the match!\n";
	}
	else if (wiz1->hp <= 0 && wiz2->hp > 0)
	{
		cout << wiz1->name << " has been defeated!\n";
		cout << wiz2->name << " wins the match!\n";
	}
	else if (wiz1->mana > 0 && wiz2->mana <= 0)
	{
		cout << wiz2->name << " has ran out of mana!\n";
		cout << wiz1->name << " wins the match!\n";
	}
	else if (wiz1->mana <= 0 && wiz2->mana > 0)
	{
		cout << wiz1->name << " has ran out of mana!\n";
		cout << wiz2->name << " wins the match!\n";
	}

	cout << endl << "Thank you for playing!";
	_getch();
	return 0;
}