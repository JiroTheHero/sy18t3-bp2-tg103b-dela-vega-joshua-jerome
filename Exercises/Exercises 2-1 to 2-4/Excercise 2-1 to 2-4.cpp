#include "pch.h"
#include "iostream"
#include "string.h"
#include "time.h"
#include "conio.h"

using namespace std;

void showDots()
{
	for (int i = 1; i <= 3; i++)
	{
		for (int k = 1; k <= i; k++)
		{
			cout << ". ";
		}
		cout << endl;
		_getch();
	}
}

// Exercise 2-1 Bet
int getBet(int &gold) // function that gets deducts bet value and returns bet;
{
	int bet;
	cout << "Please enter your bet: ";
	cin >> bet;

	gold -= bet;

	return bet;
}

void getBetAndDeductGold(int &gold, int &bet) // void return that gets bet value and deducts current gold;
{
	cout << "Please enter your bet: ";
	while (true)
	{
		cin >> bet;
		if (bet <= gold)
		{
			break;
		}
		else
		{
			cout << "Please enter a bet that does not exceed your current money.\n";
		}
	}
	gold -= bet;
}


//Exercise 2-2 Dice roll
void rollDice(int &dice1, int &dice2)
{
	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
}

//Exercise 2-3 Payout
void printPayoutSequence(int userDice1, int userDice2, int aiDice1, int aiDice2, int &gold, int bet)
{
	int userSum = userDice1 + userDice2;
	int aiSum = aiDice1 + aiDice2;
	cout << endl;
	cout << "You roll " << userDice1 << " - " << userDice2 << " together to get the value of " << userSum << endl;
	cout << "the AI rolls " << aiDice1 << " - " << aiDice2 << " together to get the value of " << aiSum << endl;
	cout << endl;

	if ((userDice1 == 1 && userDice2 == 1) && (aiDice1 == 1 && aiDice2 == 1))
	{
		cout << "It's a draw!\n";
		cout << "You get your money back.\n";
		gold += bet;
	}
	else if (userSum == aiSum)
	{
		cout << "It's a draw!\n";
		cout << "You get your money back.\n";
		gold += bet;
	}
	else if (userDice1 == 1 && userDice2 == 1)
	{
		cout << "You win with with snake eyes!\n";
		cout << "You win triple the value of your bet!\n";
		gold += (bet * 3);
	}
	else if (aiDice1 == 1 && aiDice2 == 1)
	{
		cout << "You lose. The opponent rolled snake eyes!\n";
		cout << "You lose your bet.\n";
	}
	else if (userSum > aiSum)
	{
		cout << "You win for you rolled for greater value!\n";
		cout << "You win double the value of your bet!\n";
		gold += (bet * 2);
	}
	else if (userSum < aiSum)
	{
		cout << "You lose. The opponent rolled a greater value!\n";
		cout << "You lose your bet.\n";
	}
	else
	{
		cout << "this is the error";
	}

	cout << "You have " << gold << " gold now.\n";

	_getch();
	system("cls");
}

// Exercise 2-4 Play Round
void playRound(int &gold)
{
	int bet = 0;
	int userDice1 = 0, userDice2 = 0;
	int aiDice1 = 0, aiDice2 = 0;

	cout << "Current money: " << gold << endl;
	/*bet = getBet(gold); // using the first function
	cout << bet;*/
	getBetAndDeductGold(gold, bet); //using the second function;
	cout << "You bet " << bet << " gold!\n";
	_getch();
	cout << "time to roll the dice!\n";
	rollDice(userDice1, userDice2);
	rollDice(aiDice1, aiDice2);
	_getch();
	showDots();
	printPayoutSequence(userDice1, userDice2, aiDice1, aiDice2, gold, bet);
}

int main()
{
	srand(time(NULL));
	int gold = 1000;

	cout << "What's up player! Welcome to this betting game!\n";
	cout << "You're going to start with " << gold << " gold. Spend it wisely!\n\n";
	while (gold > 0)
	{
		playRound(gold);
	}

	system("cls");
	cout << "You lost all your money. Thanks for playing!";

	_getch();
	return 0;
}