// Exercises115.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "conio.h"
#include "iostream"
#include "string"
#include "time.h"

using namespace std;

int getFactorial(int number)
{
	if (number == 0)
	{
		return 1;
	}
	else
	{
		for (int y = number; y >= 1; y--)
		{
			number = number * y;
		}
		return number;
	}
}

void printArray(string array[], int size)
{
	for (int a = 0; a < size; a++)
	{
		cout << array[a] << endl;
	}
}

void getInstances(string array[], string userInput, int size)
{
	bool haveItem = false;
	int counter = 0;

	for (int z = 0; z < size; z++)
	{
		if (userInput == array[z])
		{
			haveItem = true;
			counter++;
		}
	}
	if (haveItem)
	{
		cout << "You have " << counter << " of " << userInput << endl;
	}
	else
	{
		cout << "You do not have that item in your inventory." << endl;
	}
}

int fillRandomArray(int &numberArray)
{
	return numberArray = rand() % 100 + 1;
}

int getLargestNumber(int myArray[], int size)
{
	int largestNum;
	for (int m = 0; m < size; m++)
	{
		for (int n = 0; n < size; n++)
		{
			if (myArray[m] < myArray[n])
			{
				largestNum = myArray[n];
			}
		}
	}
	return largestNum;
}

int main()
{
	srand(time(NULL));
	// Exercise 1-1 factorial
	int num = 5;
	cout << num << "! = " << getFactorial(num) << "\n\n";

	//Exercise 1-2 PrintArray
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	int arraySize = sizeof(items) / sizeof(items[0]);
	printArray(items, arraySize);
	cout << "\n\n";

	//Exercise 1-3 Number of instances
	string input;
	cout << "Input item to check how much you have in your inventory: ";
	getline(cin, input);
	getInstances(items, input, arraySize);
	cout << "\n\n";

	//Exercise 1-4 Random Array Fill
	int randomArray[10];
	int arraySize2 = sizeof(randomArray) / sizeof(randomArray[0]);
	for (int x = 0; x < arraySize2; x++)
	{
		fillRandomArray(randomArray[x]);
		cout << randomArray[x] << " ";
	}
	cout << "\n\n";

	//Exercise 1-5 Largest Number
	cout << "The largest number in the array above is: " << getLargestNumber(randomArray, arraySize);

	_getch();
    return 0;
}