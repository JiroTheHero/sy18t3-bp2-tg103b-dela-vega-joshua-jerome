#include "pch.h"
#include "iostream"
#include "time.h"
#include "conio.h"
#include "string"

using namespace std;

void showDots()
{
	for (int i = 1; i <= 3; i++)
	{
		for (int k = 1; k <= i; k++)
		{
			cout << ". ";
		}
		cout << endl;
		_getch();
	}
}

//Exercise 3-1
struct item //struct for item
{
	string name;
	int gold;
};

item item1 = { 
	"Mithril Ore",
	100 
};

item item2 = { 
	"Sharp Talon",
	50
};

item item3 = { 
	"Thick Leather",
	25 
};

item item4 = { 
	"Jellopy",
	5
};

item item5 = { 
	"Cursed Stone",
	0 
};

void getItem(string *itemName, int *itemGold) //function for getting the item
{
	int chance = rand() % 100 + 1;

	if (chance > 80 )
	{
		*itemName = item1.name;
		*itemGold = item1.gold;
	}
	else if (chance > 60)
	{
		*itemName = item2.name;
		*itemGold = item2.gold;
	}
	else if (chance > 40)
	{
		*itemName = item3.name;
		*itemGold = item3.gold;
	}
	else if (chance > 20)
	{
		*itemName = item4.name;
		*itemGold = item4.gold;
	}
	else
	{
		*itemName = item5.name;
		*itemGold = item5.gold;
	}
}

//Exercise 3-2
void enterDungeon(int *gold) 
{
	string *itemName = new string;
	int *itemGold = new int;
	int multiplier = 1;
	int accumulatedGold = 0;
	bool isInDungeon = true;
	char userChoice;

	*gold -= 25; //dungeon cost

	do 
	{
		cout << "Current gold: " << *gold << endl;
		cout << "Accumulated gold: " << accumulatedGold << endl;
		cout << "Current Multiplier: " << multiplier << endl << endl;

		showDots();

		getItem(itemName, itemGold); // gets the item

		cout << endl;
		cout << "You have acquired a " << *itemName << "!\n";
		accumulatedGold += *itemGold * multiplier;
		cout << "You get " << *itemGold << " gold!\n";
		_getch();
		cout << endl;
		
		if (*itemGold == 0) // if player gets a cursed stone
		{ 
			cout << "Unfortunately you die and lose all your acquired gold." << endl;
			cout << "You will respawn outside the dungeon\n";
			isInDungeon = false;
			_getch();
		}
		else // when player doesn't get cursed stone
		{
			cout << "Do you want to continue do go down the dungeon? (Y/N) \n";
			bool isOptionValid = false;
			while (!isOptionValid) // choice for continuing down the dungeon
			{
				cin >> userChoice;
				if (userChoice == 'y' || userChoice == 'Y')
				{
					if (multiplier < 4)
					{
						multiplier++;
					}
					isOptionValid = true;
				}
				else if (userChoice == 'n' || userChoice == 'N')
				{
					cout << "You leave the dungeon with " << accumulatedGold << " gold with you!\n";
					*gold += accumulatedGold;
					isInDungeon = false;
					isOptionValid = true;
					_getch();
				}
				else
				{
					cout << "Choose a valid option.\n";
				}
			}
		}
		system("cls");
	} while (isInDungeon); // dungeon loop
}

int main()
{
	srand(time(NULL));

	int *gold = new int;
	*gold = 50;
	bool canStillPlay = true;
	char userInput;

	cout << "Welcome to the DanMachi dungeon simulator where it's all dungeon and no cute girls!\n";
	cout << "You start out with 50 gold.\n";
	_getch();
	system("cls");

	do
	{
		cout << "Current gold: " << *gold << endl;
		cout << "Entering the dungeon costs 25 gold\n";
		cout << "Entering dungeon... \n";
		_getch();
		system("cls");

		enterDungeon(gold);

		if (*gold < 25) // when player doesn't have enough money to play (lose condition)
		{
			cout << "You don't have enough money to continue playing.\n";
			_getch();
			canStillPlay = false;
		}
		else if (*gold >= 500) // when player gains more that 500 gold from dungeon (win condition)
		{
			cout << "You have won the game by getting more than 500 gold from the dungeons!";
			_getch();
			canStillPlay = false;
		}
		else // when player doesn't have enough to win but still has enough money to return in the dungeon
		{
			cout << "Current gold: " << *gold << endl;
			cout << "Do you want to enter the dungeon again? (Y/N)\n";
			bool isOptionValid = false;
			while (!isOptionValid)
			{
				cin >> userInput;
				if (userInput == 'y' || userInput == 'Y') // goes back to the dungeon
				{
					cout << "You walk back to the dungeon entrance...";
					isOptionValid = true;
					_getch();
					system("cls");
				}
				else if (userInput == 'n' || userInput == 'N') // added lose condition haha
				{
					cout << "You go home like a coward...\n";
					_getch();
					cout << "You get robbed and lose all your money...\n";
					_getch();
					isOptionValid = true;
					canStillPlay = false;
				}
				else
				{
					cout << "Choose a valid option\n";
				}
			}
		}
	} while (canStillPlay); // loop for continuing the game

	system("cls");
	cout << "Game Over. Thank you for Playing.\n";
	cout << "		-Jiro Productions";

	_getch();
	return 0;
}