#pragma once
#include <string>

using namespace std;

class Items
{
public:
	Items();

	string name;
	int itemMod;

	void setDagger();
	void setShortSword();
	void setBroadSword();
	void setFists();

	void setLeather();
	void setChain();
	void setPlate();
	void setCloth();
};

