#include "pch.h"
#include "GameManager.h"
#include "string"
#include "iostream"
#include "Monsters.h"
#include <conio.h>
#include <time.h>

using namespace std;

GameManager::GameManager()
{
}

void GameManager::intro()
{
	cout << "\n\n\n\n\n\n\n\n";
	cout << "	 ____           _____ _____  _____   _____ ___    _____  _____   _____ " << endl;
	cout << "	|  _ \\   /\\    / ____|  __ \\|  __ \\ / ____|__ \\  |  __ \\|  __ \\ / ____|" << endl;
	cout << "	| |_) | /  \\  | (___ | |__) | |__) | |  __   ) | | |__) | |__) | |  __ " << endl;
	cout << "	|  _ < / /\\ \\  \\___ \\|  ___/|  _  /| | |_ | / /  |  _  /|  ___/| | |_ |" << endl;
	cout << "	| |_) / ____ \\ ____) | |    | | \\ \\| |__| |/ /_  | | \\ \\| |    | |__| |" << endl;
	cout << "	|____/_/    \\_\\_____/|_|    |_|  \\_\\\\_____|____| |_|  \\_\\_|     \\_____|" << endl;
	cout << "				Brought to you by" << endl;
	cout << "				Jiro Productions" << endl;

	_getch();
	system("cls");
}

void GameManager::gameFlow(Character *player)
{
	hasQuit = false;
	while(player->hp > 0 && hasQuit != true) 
	{
		cout << "Player position: (" << player->xpos << ", " << player->ypos << ")" << endl;
		cout << "What do you want to do?" << endl;
		cout << "[1] Move  [2] Rest  [3] View Stats  [4] Quit: " << endl;
		char choice;
		while (true)
		{
			cin >> choice;
			if (choice == '1')
			{
				player->move();
				this->encounter(player);
				break;
			}
			else if (choice == '2')
			{
				player->rest();
				break;
			}
			else if (choice == '3')
			{
				player->printStats();
				break;
			}
			else if (choice == '4')
			{
				hasQuit = true;
				break;
			}
			else
			{
				cout << "Please choose a valid choice." << endl;
			}
			system("cls");
		}
	}
}

void GameManager::encounter(Character *player)
{
	int encounterChance;
	encounterChance = rand() % 5 + 1;

	if (encounterChance > 1)
	{
		Monsters *spawn = new Monsters();
		system("cls");
		cout << "you have encountered an enemy!!" << endl;
		spawn->spawnMonster();
		spawn->printMonStats();
		battle(player, spawn);
		_getch();
		delete spawn;
	}
}

void GameManager::battle(Character *player, Monsters *enemy)
{
	bool hasFled = false;
	while (!hasFled)
	{
		cout << "[1] Battle or [2] Run Away?: ";
		char choice;
		cin >> choice;
		if (choice == '1')
		{
			player->attacking(enemy);
			enemy->attacking(player);
		}
		else if (choice == '2')
		{
			int fleeChance;
			fleeChance = rand() % 4 + 1;
			if (fleeChance == 1)
			{
				cout << "you have ran away safely!" << endl;
				hasFled = true;
			}
			else
			{
				cout << "You failed to run away!" << endl;
				enemy->attacking(player);
			}
		}
		else
		{
			cout << "please choose a valid option." << endl;
		}

		if (player->hp <= 0 || enemy->hp <= 0)
		{
			break;
		}
		cout << endl;
		_getch();
	}

	if (player->hp <= 0)
	{
		cout << endl << "You died!" << endl;
		system("pause");
	}
	else if (enemy->hp <= 0)
	{
		cout << endl << "You killed the " << enemy->className << "!" << endl;
		cout << "You gain " << enemy->gold << " gold and " << enemy->exp << " exp!" << endl;
		player->gold += enemy->gold;
		player->exp += enemy->exp;
		system("pause");
	}
	
	this->levelUP(player);

	system("cls");
}

void GameManager::levelUP(Character *player)
{
	if (player->exp >= (player->lvl * 1000))
	{
		system("cls");
		player->hp = player->maxHP;

		cout << "You leveled up!!" << endl;
		player->lvl++;
		cout << "You are now level " << player->lvl << endl;
		player->hp += 5;
		player->pow += 3;
		player->vit += 2;
		player->agi += 3;
		player->dex += 2;

		cout << "Your stats are now: " << endl;
		player->printStats();
	}
}