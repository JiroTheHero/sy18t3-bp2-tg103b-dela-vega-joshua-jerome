#include "pch.h"
#include "Items.h"


Items::Items()
{
}

void Items::setDagger()
{
	this->name = "Dagger";
	this->itemMod = 2;
}

void Items::setShortSword()
{
	this->name = "Short Sword";
	this->itemMod = 3;
}

void Items::setBroadSword()
{
	this->name = "Broad Sword";
	this->itemMod = 5;
}

void Items::setFists()
{
	this->name = "Fists";
	this->itemMod = 0;
}



void Items::setLeather()
{
	this->name = "Leather Armor";
	this->itemMod = 3;
}

void Items::setChain()
{
	this->name = "Chain Mail";
	this->itemMod = 4;
}

void Items::setPlate()
{
	this->name = "Plate Mail";
	this->itemMod = 5;
}

void Items::setCloth()
{
	this->name = "Cloth armor";
	this->itemMod = 0;
}