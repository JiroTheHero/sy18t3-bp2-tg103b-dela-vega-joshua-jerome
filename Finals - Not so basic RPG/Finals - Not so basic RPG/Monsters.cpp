#include "pch.h"
#include "Monsters.h"
#include "time.h"
#include <iostream>

Monsters::Monsters()
{
}

void Monsters::setAsGoblin()
{
	plyrName = "Goblin";
	hp = 15;
	pow = 7;
	vit = 2;
	agi = 3;
	dex = 3;
	gold = 10;
	exp = 100;
}

void Monsters::setAsOgre()
{
	plyrName = "Ogre";
	hp = 20;
	pow = 9;
	vit = 3;
	agi = 4;
	dex = 4;
	gold = 50;
	exp = 250;
}

void Monsters::setAsOrc()
{
	plyrName = "Orc";
	hp = 25;
	pow = 11;
	vit = 4;
	agi = 4;
	dex = 5;
	gold = 100;
	exp = 500;
}

void Monsters::setAsOrcLord()
{
	plyrName = "Orc Lord";
	hp = 50;
	pow = 18;
	vit = 8;
	agi = 8;
	dex = 8;
	gold = 1000;
	exp = 1000;
}

void Monsters::spawnMonster()
{
	int spawnChances;
	spawnChances = rand() % 20 + 1;
	if (spawnChances == 1)
	{
		this->setAsOrcLord();
	}
	else
	{
		spawnChances = rand() % 3 + 1;
		if (spawnChances == 1)
		{
			this->setAsGoblin();
		}
		else if (spawnChances == 2)
		{
			this->setAsOgre();
		}
		else
		{
			this->setAsOrc();
		}
	}
	this->className = this->plyrName;
	this->weapon->setFists();
	this->armor->setCloth();
}

void Monsters::printMonStats()
{
	cout << "MONSTER STATS" << endl;
	cout << "=======================" << endl;
	cout << "NAME:		" << this->plyrName << endl;
	cout << "HP:		" << this->hp << endl;
	cout << "POW:		" << this->pow << endl;
	cout << "VIT:		" << this->vit << endl;
	cout << "AGI:		" << this->agi << endl;
	cout << "DEX:		" << this->dex << endl;
	cout << "=======================" << endl;
}