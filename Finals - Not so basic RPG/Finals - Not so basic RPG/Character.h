#pragma once
#include "Classes.h"
#include "Monsters.h"
#include "Dungeon.h"

class Character :
	public Classes
{
public:
	Character();

	Dungeon *playerPos = NULL;

	void makeCharacter();
	void printStats();
	void move();
	void rest();
};

