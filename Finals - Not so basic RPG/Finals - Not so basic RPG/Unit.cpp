#include "pch.h"
#include "Unit.h"
#include <time.h>
#include <iostream>

Unit::Unit()
{
}

void Unit::attacking(Unit *target)
{
	cout << this->plyrName << " attacks the " << target->className << " with their " << this->weapon->name << "!" << endl;
	int hitRate;
	hitRate = (this->dex / target->agi) * 100;
	if (hitRate > 80)
	{
		hitRate = 80;
	}
	else if (hitRate < 20)
	{
		hitRate = 20;
	}

	int hitChance;
	hitChance = rand() % 100 + 1;

	if (hitRate >= hitChance)
	{
		cout << "The attack hits!" << endl;
		int damage;
		damage = (this->pow + this->weapon->itemMod) - (target->vit + target->armor->itemMod);
		if (damage < 1)
		{
			damage = 1;
		}
		target->hp -= damage;
		cout << damage << " damage has been dealt!" << endl;
	}
	else
	{
		cout << "The attack misses!" << endl;
	}
}
