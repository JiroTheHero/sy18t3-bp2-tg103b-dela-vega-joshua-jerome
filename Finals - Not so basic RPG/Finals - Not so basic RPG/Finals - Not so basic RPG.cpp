#include "pch.h"
#include <iostream>
#include "Character.h"
#include "GameManager.h"
#include <conio.h>
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	Character *player = new Character();
	GameManager *startGame = new GameManager();

	startGame->intro();
	player->makeCharacter();
	startGame->gameFlow(player);

	cout << "Game Over. Thank you for playing!!" << endl;
	cout << "Jiro Productions" << endl;

	_getch();
}