#pragma once
#include <string>
#include "Items.h"

using namespace std;

class Unit
{
public:
	Unit();
	
	string plyrName;
	string className;
	int hp;
	int maxHP;
	int pow;
	int vit;
	int agi;
	int dex;
	int gold;
	int exp;
	int lvl;
	int xpos;
	int ypos;

	Items *weapon = new Items;
	Items *armor = new Items;

	void attacking(Unit *target);
};

