#include "pch.h"
#include "Classes.h"
#include "Items.h"


Classes::Classes()
{
}

void Classes::setAsKnight()
{
	this->className = "Knight";
	this->hp = 30;
	this->pow = 13;
	this->vit = 5;
	this->agi = 2;
	this->dex = 3;
	
	this->weapon->setBroadSword();
	this->armor->setPlate();
}

void Classes::setAsBarbarian()
{
	this->className = "Barbarian";
	this->hp = 25;
	this->pow = 15;
	this->vit = 4;
	this->agi = 3;
	this->dex = 4;

	this->weapon->setShortSword();
	this->armor->setChain();
}

void Classes::setAsThief()
{
	this->className = "Thief";
	this->hp = 20;
	this->pow = 10;
	this->vit = 2;
	this->agi = 5;
	this->dex = 5;

	this->weapon->setDagger();
	this->armor->setLeather();
}