#pragma once
#include "Unit.h"

class Monsters :
	public Unit
{
public:
	Monsters();

	void setAsGoblin();
	void setAsOgre();
	void setAsOrc();
	void setAsOrcLord();

	void spawnMonster();
	void printMonStats();
};

