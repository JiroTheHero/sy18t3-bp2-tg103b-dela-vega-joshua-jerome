#include "pch.h"
#include "Character.h"
#include <string.h>
#include <iostream>
#include <conio.h>
#include "Monsters.h"
#include <time.h>
#include "Items.h"

using namespace std;

Character::Character()
{
}

void Character::makeCharacter()
{
	string plyr;
	cout << "Welcome to your adventure!!" << endl;
	cout << "What is your name adventurer?: ";
	cin >> plyr;
	Unit::plyrName = plyr;
	cout << endl;
	cout << "Choose your class!\n";
	cout << "[1] Knight  [2] Thief  [3] Barbarian\n";
	char choice;
	while (true)
	{
		cin >> choice;
		if (choice == '1')
		{
			this->setAsKnight();
			Items *weapon = new Items;
			weapon->setBroadSword();
			Items *armor = new Items;
			armor->setPlate();
			break;
		}
		else if (choice == '2')
		{
			this->setAsThief();
			break;
		}
		else if (choice == '3')
		{
			this->setAsBarbarian();
			break;
		}
		else
		{
			cout << "please choose a valid choice." << endl;
		}
	}
	this->maxHP = this->hp;
	this->lvl = 1;
	this->gold = 0;
	this->exp = 0;
	this->xpos = 0;
	this->ypos = 0;
}

void Character::printStats()
{
	cout << "CHARACTER STATS" << endl;
	cout << "=======================" << endl;
	cout << "NAME:			" << this->plyrName << endl;
	cout << "CLASS:			" << this->className << endl;
	cout << "LEVEL:			" << this->lvl << endl;
	cout << "HP:			" << this->hp << endl;
	cout << "POW:			" << this->pow << endl;	
	cout << "VIT:			" << this->vit << endl;
	cout << "AGI:			" << this->agi << endl;
	cout << "DEX:			" << this->dex << endl;
	cout << "WEAPON:			" << this->weapon->name << " | Attack Power: " << this->weapon->itemMod <<endl;
	cout << "ARMOR:			" << this->armor->name << " | Defense: " << this->armor->itemMod << endl;
	cout << "GOLD:			" << this->gold << endl;
	cout << "CURRENT EXP:		" << this->exp << endl;
	cout << "EXP TO NEXT LEVEL:	" << lvl*1000 << endl;
	cout << "=======================" << endl;

	system("pause");
	system("cls");
}

void Character::rest()
{
	cout << "You rest and gain back all your HP" << endl;
	this->hp = this->maxHP;

	_getch();
	system("cls");
}

void Character::move()
{
	cout << "Where do you want to go?" << endl;
	cout << "[1] North  [2] West  [3] East  [4] South" << endl;
	char choice;
	while (true)
	{
		cin >> choice;
		if (choice == '1')
		{
			ypos++;
			break;
		}
		else if (choice == '2')
		{
			xpos--;
			break;
		}
		else if (choice == '3')
		{
			xpos++;
			break;
		}
		else if (choice == '4')
		{
			ypos--;
			break;
		}
		else
		{
			cout << "Please choose a valid choice." << endl;
		}
	}

	system("cls");
}