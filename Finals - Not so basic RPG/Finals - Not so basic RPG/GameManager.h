#pragma once
#include "Character.h"
#include "Dungeon.h"
#include "Unit.h"

class GameManager
{
public:
	GameManager();
	
	bool hasQuit;

	void intro();
	void gameFlow(Character *player);
	void encounter(Character *player);
	void battle(Character *player, Monsters *enemy);
	void levelUP(Character *player);
};

