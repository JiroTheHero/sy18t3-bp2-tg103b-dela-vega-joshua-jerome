#include "pch.h"
#include "iostream"
#include "vector"
#include "conio.h"
#include "string"
#include "time.h"

using namespace std;

void fillEmperorHand(vector<string> &cardList)//fills vector with emperor hand
{
	cardList.push_back("Emperor");
	for (int i = 0; i < 4; i++)
	{
		cardList.push_back("Citizen");
	}
}

void fillSlaveHand(vector<string> &cardList) //fills vector with slave hand
{
	cardList.push_back("Slave");
	for (int i = 0; i < 4; i++)
	{
		cardList.push_back("Citizen");
	}
}

void printCardList(vector<string> cardList, int size) //prints value of vector list
{
	for (int i = 0; i < size; i++)
	{
		cout << "[" << i + 1 << "] " << cardList[i] << endl;
	}
}

void distributeCards(vector<string> &playerHand, vector<string> &aiHand, int round) //distributes card list depending on the round#
{
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		fillEmperorHand(playerHand);
		fillSlaveHand(aiHand);
	}
	else
	{
		fillSlaveHand(playerHand);
		fillEmperorHand(aiHand);
	}
}

int getMultiplier(int round, int wager) //returns multiplier value
{
	int multiplier;
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		return multiplier = 1 * wager;
	}
	else
	{
		return multiplier = 5 * wager;
	}
}

void printPlayerSide(int round) //prints what side the player is on depending on the round#
{
	if (round <= 3 || (round >= 7 && round <= 9))
	{
		cout << "Side: Emperor" << endl;
	}
	else
	{
		cout << "Side: Slave" << endl;
	}
}

void getWager(int &availableDist, int &wageredDist) //gets wager
{
	while (true)
	{
		cout << "How many mm would you like to wager, Kaiji? ";
		cin >> wageredDist;
		if (wageredDist <= availableDist)
		{
			break;
		}
	}
}

int chooseCard(int handSize) //prompts player to choose from the card list
{
	int cardChoice;
	while (true)
	{
		cin >> cardChoice;
		if (cardChoice <= handSize)
		{
			break;
		}
		cout << "please enter a valid card number. ";
	}
	return cardChoice;
}

void evaluateMatchAndGetPayback(vector<string> &playerHand, vector<string> &aiHand, int playerChoice, int aiChoice, int &gold, int multiplier, int &mmLeft, int mmWagered, bool &ifDraw) //Evaluates card match-up
{
	string playerCard = playerHand[playerChoice];
	string aiCard = aiHand[aiChoice];
	int goldAccumulated = 0;

	cout << "Open!\n\n";
	cout << "[Kaiji] " << playerCard << " vs [Tonegawa] " << aiCard << endl << endl;
	if (playerCard == "Slave" && aiCard == "Emperor") //win
	{
		goldAccumulated = 100000 * multiplier;
		gold += goldAccumulated;
		cout << "You won " << goldAccumulated << " yen.\n";
		ifDraw = false;
	}
	else if (playerCard == "Emperor" && aiCard == "Slave") //lose
	{
		mmLeft -= mmWagered;
		cout << "You lose!\n";
		cout << "Bzzzzz the drill moves " << mmWagered << "mm closer to your eardrum...\n";
		ifDraw = false;
	}
	else if (playerCard == "Emperor" && aiCard == "Citizen") //win
	{
		goldAccumulated = 100000 * multiplier;
		gold += goldAccumulated;
		cout << "You won " << goldAccumulated << " yen.\n";
		ifDraw = false;
	}
	else if (playerCard == "Slave" && aiCard == "Citizen") //lose
	{
		mmLeft -= mmWagered;
		cout << "You lose!\n";
		cout << "Bzzzzz the drill moves " << mmWagered << "mm closer to your eardrum...\n";
		ifDraw = false;
	}
	else if (playerCard == "Citizen" && aiCard == "Slave") //win
	{
		goldAccumulated = 100000 * multiplier;
		gold += goldAccumulated;
		cout << "You won " << goldAccumulated << " yen.\n";
		ifDraw = false;
	}
	else if (playerCard == "Citizen" && aiCard == "Emperor") //lose
	{
		mmLeft -= mmWagered;
		cout << "You lose!\n";
		cout << "Bzzzzz the drill moves " << mmWagered << "mm closer to your eardrum...\n";
		ifDraw = false;
	}
	else //draw
	{
		cout << "It's a draw!\n";
		ifDraw = true;
		playerHand.erase(playerHand.begin() + playerChoice);
		aiHand.erase(aiHand.begin() + aiChoice);
	}
	system("pause");
}

void printEnding(int mmWagered, int money) //prints end result of game
{
	if (mmWagered > 0 && money >= 20000000) //best ending
	{
		cout << "Congratulations! You have gained 20,000,000 yen.\n";
		cout << "You have shown Tonegawa that you are the boss as you defeat him in his own game.\n";
		cout << "You feel great as you have avenged your friends and have enough money to pay your debt. \n";
	}
	else if (mmWagered > 0 && money < 20000000) //meh ending
	{
		cout << "You did not reach 20,000,000 yen\n";
		cout << "Tonegawa laughs as you dig yourself into despair.\n";
		cout << "You will be forever haunted by the souls those who died.\n";
	}
	else if (mmWagered <= 0 && money < 20000000) // bad ending
	{
		cout << "The pin has pierced your eardrum.\n";
		cout << "No matter how much money you've won, it will not bring back what you've lost.\n";
		cout << "Tonegawa will not be kneeling down on a loser like you.\n";
	}
}

int main()
{
	srand(time(NULL));
	vector<string> playerHand;
	vector<string> aiHand;
	int handSize;
	int round = 1;
	int moneyGained = 0;
	int mmToWager = 30;
	int wageredDistance;
	int multiplier;
	bool ifDraw = false;

	while (round <= 12 && mmToWager > 0) // loop for the 12 rounds
	{
		cout << "Cash: " << moneyGained << endl;
		cout << "Distance Left: " << mmToWager << endl;
		cout << "Round: " << round << "/12" << endl;
		printPlayerSide(round);
		cout << endl;

		getWager(mmToWager, wageredDistance); // gets wager
		distributeCards(playerHand, aiHand, round); // distributes card to player and opponent depending on round#
		multiplier = getMultiplier(round, wageredDistance);
		system("cls");

		do //loop for the round sequence
		{
			cout << "Pick a card to play... \n";
			cout << "=======================\n";
			handSize = playerHand.size(); //returns player hand size
			printCardList(playerHand, handSize); //prints player hand list
			int playerChoice = chooseCard(handSize) - 1; //returns chosen index number from vector
			int aiChoice = rand() % aiHand.size();
			system("cls");
			evaluateMatchAndGetPayback(playerHand, aiHand, playerChoice, aiChoice, moneyGained, multiplier, mmToWager, wageredDistance, ifDraw); //evaluates the card match-up
			system("cls");
		} while (ifDraw == true && handSize > 1);

		playerHand.clear(); //clears both vectors
		aiHand.clear();
		round++;
	}

	printEnding(mmToWager, moneyGained); //prints end result

	cout << endl;
	system("pause");
	return 0;
}