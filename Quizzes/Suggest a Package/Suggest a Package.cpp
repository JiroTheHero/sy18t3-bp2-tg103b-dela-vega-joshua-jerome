#include "pch.h"
#include "conio.h"
#include "iostream"

using namespace std;

void sortArray(int myArray[], int size)
{
	//using a temporary variable for swapping.
	int tempVar;
	for (int i = 0; i < size; i++)
	{
		for (int k = i + 1; k < size; k++)
		{
			if (myArray[i] > myArray[k])
			{
				/*swap(myArray[i], myArray[k]);*/ //I wanted to use the swap function, but i didn't know if it was completely allowed.
				tempVar = myArray[i];
				myArray[i] = myArray[k];
				myArray[k] = tempVar;
			}
		}
	}
}

void printSuggestPackage(int packages[], int size, int& money, int price)
{
	int moneyPlus;
	for (int i = 0; i < size; i++)
	{
		//algorithim for suggesting the minimum package deal needed
		moneyPlus = money + packages[i];
		if (moneyPlus >= price)
		{
			cout << "You don't have enough money for that. Do you want to buy package#" << i + 1 << " for additional " << packages[i] << " gold?(Y/N): \n";
			char userChoice;
			while (true) //will loop if the user has a wrong input
			{
				cin >> userChoice;
				if (userChoice == 'y' || userChoice == 'Y')
				{
					cout << "Thanks for purchasing the package! " << packages[i] << " gold will be added to your account then will automatically purchase your item. \n";
					money = moneyPlus;
					money -= price;
					break;
				}
				else if (userChoice == 'n' || userChoice == 'N')
				{
					break;
				}
				else
				{
					cout << "Not a valid input. Please try again. \n";
				}
			}
			break;
		}
	}
}

int main()
{
	bool willLoopProg = true;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050};
	int packagesSize = 7;
	int currentGold = 250;

	sortArray(packages, packagesSize);

	//loop for the main program till the user terminates
	while (willLoopProg) {
		//display packages
		cout << "Welcome to the Gacha trap gold store!\n";
		cout << "Available gold packages:\n";
		for (int i = 0; i < packagesSize; i++)
		{
			cout << "Gold chest#" << i + 1 << ": " << packages[i] << endl;
		}

		//will loop if the user inputs an amount that is too high
		while (true)
		{
			cout << endl;
			cout << "Current Gold: " << currentGold << endl;
			cout << "Input price of the item to buy from the shop: ";
			int price;
			cin >> price;
			if (currentGold >= price) //having the right amount of money for the price
			{
				currentGold -= price;
				cout << "Item purchased!\n";
				break;
			}
			else if (currentGold < price && currentGold + packages[6] >= price) //price is too expensive but can be attained by buying a package, so will go to the suggest package function 
			{
				printSuggestPackage(packages, packagesSize, currentGold, price);
				break;
			}
			else //price is too high and will continue to loop until the user doesn't input a package too high.
			{
				cout << "Sorry, there is no package available to help you purchase your item. Please input another price.\n";
			}
		}

		// money after transaction
		cout << "Your current gold now is: " << currentGold << endl;
		cout << "Do you want to shop again?(Y/N) \n";
		char userAns;
		while (true) //will loop if the user has a wrong input
		{
			cin >> userAns;
			if (userAns == 'y' || userAns == 'Y')
			{
				system("cls");
				break;
			}
			else if (userAns == 'n' || userAns == 'N')
			{
				willLoopProg = false;
				break;
			}
			else
			{
				cout << "Not a valid input. Please try again. \n";
			}
		}
	}

	//end of program
	cout << endl << "Thank you for shopping with us!" << endl;
	_getch();
	return 0;
}