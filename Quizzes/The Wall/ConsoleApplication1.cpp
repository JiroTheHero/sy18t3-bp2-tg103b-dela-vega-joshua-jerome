#include "pch.h"
#include "iostream"
#include "conio.h"
#include "time.h"

#ifndef NODE_H
#define NODE_H

#include <string>

using namespace std;

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif //NODE_H

// Node* createNodes(int size)
// loops through size.
// creates a new node for every loop
// remember to link nodes via prev->next = createdNode;

Node* createNodes(int size)
{
	Node* head = new Node;
	Node* prev = head;
	for (int i = 1; i < size; i++)
	{
		Node* createdNode = new Node;
		prev->next = createdNode;
		prev = prev->next;
	}
	prev->next = head;
	return head;
}

Node* addSoldier(Node* soldiers, string input) //Add soldier function
{
	soldiers->name = input;
	soldiers = soldiers->next;

	return soldiers;
}

void printNames(Node *soldiers, int amount)// prints names
{
	for (int i = 0; i < amount; i++)
	{
		cout << soldiers->name << endl;
		soldiers = soldiers->next;
	}
}

Node* pickStartingPoint(Node *soldiers, int start) //picks the starting point
{
	for (int i = 0; i < start; i++)
	{
		soldiers = soldiers->next;
	}
	return soldiers;
}

void getAndPrintElim(Node* soldiers, int elim) //gets and prints the eliminated soldier
{
	Node *temp = soldiers;
	for (int i = 0; i < elim; i++)
	{
		temp = temp->next;
	}
	cout << temp->name << " was eliminated.\n";
}

Node* eliminteSoldier(Node *soldiers, int elim) //eliminates soldier and patches the link
{
	Node *prev = new Node;

	for (int i = 0; i < elim; i++) //finds the value that needs to be eliminated
	{
		prev = soldiers;
		soldiers = soldiers->next;
	}

	prev->next = soldiers->next;
	delete soldiers;
	prev = prev->next;
	return prev;
}

int main()
{
	srand(time(NULL));

	int plyrNum;
	int round = 1;
	Node *soldier1 = new Node;

	cout << "Enter the amount of soldiers: \n"; //gets soldier amount;
	cin >> plyrNum;
	soldier1 = createNodes(plyrNum); //creates the list with the amount of soldiers

	cout << "Enter the name of your " << plyrNum << " soldiers!\n"; //inputs the names
	for (int i = 1; i <= plyrNum; i++)
	{
		string input;
		cin >> input;
		soldier1 = addSoldier(soldier1, input);
	}

	int start = rand() % plyrNum + 1;
	soldier1 = pickStartingPoint(soldier1, start); // picks starting point
	cout << endl;

	while (plyrNum > 1) //loop till 1 player is left
	{
		cout << "=================================================\n";
		cout << "ROUND " << round << endl;
		cout << "=================================================\n";
		cout << "Remaining Soldiers: \n";
		printNames(soldier1, plyrNum); //prints names
		cout << endl;

		_getch();
		int elim = rand() % plyrNum + 1; //rolls for player to be eliminated
		cout << "Result: \n";
		_getch();
		cout << soldier1->name << " has drawn " << elim << endl;
		getAndPrintElim(soldier1, elim); //to show who got eliminated
		soldier1 = eliminteSoldier(soldier1, elim); //takes out the eliminated player off the list

		plyrNum--; //increments for the loop
		round++;

		_getch();
		system("cls");
	}

	cout << "=================================================\n";
	cout << "FINAL RESULT" << endl;
	cout << "=================================================\n";
	_getch();
	cout << soldier1->name << " will go seek for reinforcements.\n"; //final result
	_getch();
	system("cls");

	cout << "Game Over. Thank you for Playing.\n";
	cout << "		-Jiro Productions";

	_getch();
	return 0;
}